" required!
Bundle 'gmarik/vundle'

" Sensible defaults
Bundle 'tpope/vim-sensible'

" Preferred colorscheme
Bundle 'altercation/vim-colors-solarized'

" Powerline
Bundle 'bling/vim-airline'

" Editor helpers
Bundle 'tpope/vim-commentary'
Bundle 'tpope/vim-endwise'
Bundle 'tpope/vim-fugitive'
Bundle 'tpope/vim-markdown'
Bundle 'tpope/vim-sleuth'
Bundle 'tpope/vim-surround'
Bundle 'scrooloose/nerdcommenter'
Bundle 'scrooloose/nerdtree'
Bundle 'scrooloose/syntastic'
Bundle 'majutsushi/tagbar'

" Language support
" C
Bundle 'Rip-Rip/clang_complete'
" Python
"Bundle 'klen/python-mode'
Bundle 'hdima/python-syntax'
Bundle 'hynek/vim-python-pep8-indent'
Bundle 'nvie/vim-flake8'
" MATLAB
Bundle 'sgeb/vim-matlab'
" LaTeX
Bundle 'LaTeX-Box-Team/LaTeX-Box'

" Vundle usage
"
" :BundleList          - list configured bundles
" :BundleInstall(!)    - install(update) bundles
" :BundleSearch(!) foo - search(or refresh cache first) for foo
" :BundleClean(!)      - confirm(or auto-approve) removal of unused bundles
"
" see :h vundle for more details or wiki for FAQ
" NOTE: comments after Bundle command are not allowed.
